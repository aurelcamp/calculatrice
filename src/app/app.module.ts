import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { StepsComponent } from './steps.component';
import { CalculService } from './calcul.service';
import {Ng2Webstorage} from 'ng2-webstorage';

@NgModule({
  declarations: [
    AppComponent,
    StepsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    Ng2Webstorage
  ],
  providers: [CalculService],
  bootstrap: [AppComponent]
})
export class AppModule { }
