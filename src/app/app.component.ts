import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { StepsComponent } from './steps.component';
import { Operator, _ }  from './constants';
import {CalculService} from './calcul.service';
import {LocalStorageService} from 'ng2-webstorage';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  @ViewChild(StepsComponent) stepsComponent;

  private nbSteps = 20;
  private numberOfLevels = 3;

  private numberOfTableLines = 10;
  private numberOfTableColumns = 10;
  private numbersTable: number[][];

  private numbersPlayed: number[];

  private calculText: string;
  private result: number;

  private operator: Operator;
  private level: number;

  private levels: number[];
  private operators: any[];

  private correctAnswers: number;
  private wrongAnswers: number;
  private currentWrongAnswers: number;

  constructor(private calculService: CalculService, private elRef: ElementRef, private localStorage:LocalStorageService) {

  }

  ngOnInit(): void {

    // Generate operators array useful for html
    let numberOfOperators = Object.keys(Operator).length / 2;
    this.operators = Array(numberOfOperators).fill(0).map((x,i) => {
      let operator = Operator[Operator[i]];
      let obj = {
        operator: operator
      };
      switch(operator) {
        case Operator.Multiplication: {
          obj['string'] = 'x';
          break;
        }
        case Operator.Addition: {
          obj['string'] = '+';
          break;
        }
        case Operator.Soustraction: {
          obj['string'] = '-';
          break;
        }
        case Operator.Mix: {
          obj['string'] = '?';
          break;
        }
      }
      return obj;
    });

    // Generate levels array useful for html
    this.levels = Array(3).fill(0).map((x,i)=>i+1);

    this.level = this.localStorage.retrieve('level') || 1;
    this.operator = this.localStorage.retrieve('operator') || Operator.Multiplication;

    this.newGame();
  }

  private newGame() {
    this.correctAnswers = 0;
    this.wrongAnswers = 0;
    this.generateNumbersTable();

    this.go();
  }

  private generateNumbersTable():void {
    this.numbersTable = [];
    for (let i=0; i<this.numberOfTableLines; i++) {
      this.numbersTable.push([]);
      for (let j=0; j<this.numberOfTableColumns; j++) {
        this.numbersTable[i].push(i*10+j);
      }
    }
  }

  private setOperator(op: Operator): void {
    this.operator = op;
    this.localStorage.store('operator', op);
    this.go();
  }

  private setLevel(lvl: number): void {
    this.level = lvl;
    this.localStorage.store('level', lvl);
    this.go();
  }

  private go(): void {
    this.currentWrongAnswers = 0;
    this.numbersPlayed = [];

    var caseNumbers = this.elRef.nativeElement.querySelectorAll('.case-number');
    for (let caseNumber of caseNumbers) {
      caseNumber.classList.remove('bg-ok', 'bg-ko', 'bg-out');
    }

    let calculElements = this.calculService.getCalculElements(this.operator, this.level);
    this.result = calculElements.result;
    this.calculText = calculElements.operation;
  }

  private testResponse(response: number, caseNumber: any): void {
    this.numbersPlayed.push(response);

    let isOk = (response === this.result);
    if (isOk) {
      this.correctAnswers++;
      caseNumber.classList.add('bg-ok');
      setTimeout(() => this.go(), 600);

    } else {
      this.wrongAnswers++;
      caseNumber.classList.add('bg-ko');
      this.currentWrongAnswers++;

      if (this.currentWrongAnswers%2 === 0) {
        this.helpMe();
      }
    }

    this.stepsComponent.next(isOk);
  }

  private helpMe() {
    let flattenNumbersTable = _.flatten(this.numbersTable);

    let possibilities = flattenNumbersTable
      .filter((x) => (x !== this.result && this.numbersPlayed.indexOf(x) === -1));

    let numbersOutLength = Math.min(possibilities.length, Math.floor(flattenNumbersTable.length * 30 / 100));
    for (let i=0; i<numbersOutLength; i++) {
      let indexRand = Math.floor(Math.random() * possibilities.length);
      let randNumber = possibilities[indexRand];
      var caseNumber = this.elRef.nativeElement.querySelector('.case-number-'+randNumber);
      caseNumber.classList.add('bg-out');
      this.numbersPlayed.push(randNumber);
      possibilities.splice(indexRand, 1);
    }

  }

}
