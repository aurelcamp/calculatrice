import {} from 'jasmine';

import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement }    from '@angular/core';

import { AppComponent } from './app.component';
import {CalculService} from './calcul.service';
import {LocalStorageService} from 'ng2-webstorage';
import { StepsComponent } from './steps.component';

import {Operator, CalculElements}  from './constants';

import { async } from '@angular/core/testing';


describe('AppComponent test', () => {

  let comp:    AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  let calculServiceMock: {
    getCalculElements(operator: Operator): CalculElements
  };
  let calculService: CalculService;

  beforeEach(async(() => {

    calculServiceMock = {
      getCalculElements(operator: Operator): CalculElements {
        return {digit1:5, digit2: 7, result: 35, operation: '5 x 7'};
      }
    }

    TestBed.configureTestingModule({
      declarations: [ AppComponent, StepsComponent ],
      providers:    [
        {provide: CalculService, useValue: calculServiceMock },
        LocalStorageService
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    comp = fixture.componentInstance;

    calculService = fixture.debugElement.injector.get(CalculService);

  }));

  it('should display the calcul', () => {
    fixture.detectChanges();

    let calculText = fixture.debugElement.query(By.css('.operation .text')).nativeElement;
    expect(calculText.textContent).toContain('5 x 7');
  });

  it('should correctly react at a good answer', async(() => {
    fixture.detectChanges();

    // change the value of the default CalculElements
    calculService.getCalculElements = function(op) {
      return {digit1:4, digit2: 6, result: 24, operation: '4 x 6'};
    }

    let calculText = fixture.debugElement.query(By.css('.operation .text')).nativeElement;

    // click on the good html element
    let calculCaseCorrectElement = fixture.debugElement.query(By.css('.case-number-35')).nativeElement;
    calculCaseCorrectElement.click();

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      // new calcul launched
      expect(calculText.textContent).toContain('4 x 6');

      // score pass to 1
      let correctAnswersElement = fixture.debugElement.query(By.css('.score .ok')).nativeElement;
      expect(correctAnswersElement.textContent).toContain('1');
    });

  }));


});
