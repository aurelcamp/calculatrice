import { Injectable } from '@angular/core';

import {Operator, CalculElements}  from './constants';

@Injectable()
export class CalculService {

  getCalculElements(operator: Operator, level:number): CalculElements {

    let coefs = [0.4, 0.7, 1];
	  let coef = coefs[level-1];

    let digit1, digit2, result, operation;

    switch (operator) {
      case Operator.Multiplication: {
        digit1 = Math.floor( Math.random() * 10 );
    		digit2 = Math.floor( 1 + Math.random() * 10 );
    		digit1 = Math.floor(digit1 * coef);
    		result = digit1 * digit2;
        operation = digit1.toString() + ' x ' + digit2.toString();
        break;
      }
      case Operator.Addition: {
        digit1 = Math.floor( Math.random() * 99 );
    		digit2 = Math.floor( 1 + Math.random() * (99 - digit1) );
    		digit1 = Math.floor(digit1 * coef);
    		digit2 = Math.floor(digit2 * coef);
    		result = digit1 + digit2;
        operation = digit1.toString() + ' + ' + digit2.toString();
        break;
      }
      case Operator.Soustraction: {
        digit1 = Math.floor( 10 + Math.random() * 90 );
    		digit2 = Math.floor( Math.random() * digit1 );
    		digit1 = Math.floor(digit1 * coef);
    		digit2 = Math.floor(digit2 * coef);
        operation = digit1.toString() + ' - ' + digit2.toString();
    		result = digit1 - digit2;
        break;
      }
      case Operator.Mix: {
        let numberOfOperators = Object.keys(Operator).length / 2;
        let randomOperatorIndex = Math.floor(Math.random() * numberOfOperators);
        let randomOperator = Operator[Operator[randomOperatorIndex]];
        return this.getCalculElements(randomOperator, level);
      }
    }
    return {digit1: digit1, digit2: digit2, operation: operation, result: result};
  }


}
