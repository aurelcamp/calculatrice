export enum Operator {
  Multiplication,
  Addition,
  Soustraction,
  Mix
}

export interface CalculElements {
    digit1: number,
    digit2: number,
    operation: string,
    result: number
}

export const _ =  {
  flatten(arr: any): number[] {
    return arr.reduce(
      (a, b) => a.concat(Array.isArray(b) ? _.flatten(b) : b), []
    );
  }
}
