import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'steps-answers',
  template: `
    <ul>
      <li *ngFor="let step of steps;let i=index;" class="step"
        [class.correct]="answers.length > i && answers[i]"
        [class.wrong]="answers.length > i && !answers[i]">
      </li>
    </ul>
  `,
  styles: [`
    ul {
      padding:0;
      margin:0;
    }
    .step {
      display:inline-block;
    	margin:1%;
    	border:1px solid #666;
    	border-radius:100%;
    	width:2%;
    	padding-top:2%;
    	background-color:#FFF;
    	list-style: none;

      box-sizing: content-box;
      -moz-box-sizing: content-box;
      -webkit-box-sizing: content-box;
    }
    .step.correct {
      background-color:#4F4 !important;
    }
    .step.wrong {
      background-color:#F44 !important;
    }
  `]

})
export class StepsComponent implements OnInit {

  @Input() nbSteps: number;

  private steps: number[];
  private answers: boolean[];

  constructor() {

  }

  ngOnInit() {
    this.answers = [];
    this.steps = Array(this.nbSteps).fill(0);
  }

  next(isOk: boolean) {
    if (this.answers.length >= this.nbSteps) {
      this.answers = [];
    }
    this.answers.push(isOk);
  }


}
