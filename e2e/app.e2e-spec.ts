import { CalculatricePage } from './app.po';

describe('calculatrice App', function() {
  let page: CalculatricePage;

  beforeEach(() => {
    page = new CalculatricePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
